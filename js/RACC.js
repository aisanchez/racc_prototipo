$(document).ready(function () {
    var data = require('./js/DataProductos.json');
    $('#TablaRamos tbody tr').click(function () {
        var ramo = $(this).children().attr("id");
        $.each(data.ServiciosRamo, function (key, val) {
            if (key == ramo) {
                $('#TablaServicios tbody').html("");
                $.each(val, function (servName, servicio) {
                    $('#TablaServicios tbody').append("<tr onclick='IncluirServicio(this)' id='" + ramo + "'><td id='" + servName + "'>" + servicio.descripcion + "</td></tr>");
                });
            }
        });
    });
    $('#Tarificar').click(function () {
        $('.removeAlTarificar').attr("hidden", true);
        $('.ShowAlTarificar').removeAttr("hidden");
        $('#instruccion').text("Los Proveedores de los servicios");
    });

    $('#Finalizar').click(function () {
        $('#instruccion').text("Cálculo Prima");
        $('.hiddeAlFinalizar').attr("hidden", true);
        $('.ShowAlfinalizar').removeAttr("hidden");
        var sum = 0;
        // iterate through each td based on class and add the values
        $(".primaTotal").each(function () {

            var value = $(this).text();
            // add only if the value is number
            if (!isNaN(value) && value.length != 0) {
                sum += parseFloat(value);
            }
            $('#totalPrimaPrint').text(sum);
        });
        $('.darFormato').each(function(){
            var value = darformatoReturn($(this).text());
            $(this).text(value);
        });
    });

    $('#Calculo').click(function () {
        $('.ShowAlfinalizar').attr("hidden", true);
        $('.hiddeAlFinalizar').removeAttr("hidden");
        $('.darFormato').each(function(){
            formatoAmericano($(this));
        });
    });

    $('#Inicio').click(function () {
        $('.ShowAlTarificar').attr("hidden", true);
        $('.removeAlTarificar').removeAttr("hidden");
    });

    $('[data-toggle="tooltip"]').tooltip();
});

function IncluirServicio(rowSelected) {
    var idVerificar = $(rowSelected).attr("id");
    var servicioVerificar = $(rowSelected).children().attr("id");
    if ($('#TablaProducto tbody tr#' + idVerificar + ' td#' + servicioVerificar).length <= 0) {
        var row = $(rowSelected).clone();
        $(row).attr("onClick", "removeRow(this)");
        $(row).attr("data-toggle", "tooltip");
        $(row).attr("data-placement", "top");
        $(row).attr("title", "Click para remover!");
        $(row).removeClass("ServicioIncluido");
        var ramo = $(row).attr("id");
        var servicio = $(row).children().attr("id");
        var data = require('./js/DataProductos.json');
        var primaRacc = data.ServiciosRamo[ramo][servicio]["PrimaRACC"];
        var primaOtros = data.ServiciosRamo[ramo][servicio]["PrimaOtros"];
        var Costes = data.ServiciosRamo[ramo][servicio]["Costes"];
        var Impuestos = data.ServiciosRamo[ramo][servicio]["Impuestos"];
        var Rowtarificador = ContruirRowTarificador(primaRacc, primaOtros, servicio, ramo);
        $('#TablaProducto tbody').append(row);
        $(rowSelected).addClass("ServicioIncluido");
        $('#TablaTarificacion tbody').append(Rowtarificador);
        ConstruirRowTarifaFinal(row);
    }
}

function CalcularPrimaNeta(primaRiesgo, Costes) {
    return parseFloat(primaRiesgo) + parseFloat(Costes);
}

function CalcularPrimaTotal(primaNeta, Impuestos) {
    return parseFloat(primaNeta) + parseFloat(Impuestos);
}

function removeRow(row) {
    var ramo = $(row).attr("id");
    var servicio = $(row).children().attr("id");
    var id1 = "tariFin-" + ramo + "-" + servicio;
    var id2 = "tari-" + ramo + "-" + servicio;
    $(row).remove();
    $('#TablaTarificacionFinal tbody tr#' + id1).remove();
    $('#TablaTarificacion tbody tr#' + id2).remove();
}

function ContruirRowTarificador(PrimaRACC, PrimaOtros, servicio, ramo) {
    var row = "<tr id='tari-" + ramo + "-" + servicio + "'><td style='text-align:center;'>" + ConstruirCheckBox(PrimaRACC, servicio, ramo) + "</td><td style='text-align:center;'>" + ConstruirCheckBox(PrimaOtros, servicio, ramo) + "</td></tr>";
    return row;
}

function ContruirInputPrimaServicio(servicio) {
    var input = "<input type='text' style='width:110px; height:25px; border:solid 1px black;'>";
    return input;
}

function ConstruirCheckBox(prima, servicio, ramo) {
    var check = '<input onchange="SeleccionarPrimaServicio(this)" type="checkbox" id="' + servicio + '" name="' + ramo + "-" + servicio + '" value="' + prima + '" class="form-check-input">';
    return check;
}

function UpdateRowTarifaFinal(primaRiesgo, row) {
    var data = require('./js/DataProductos.json');
    var Costes = data.ServiciosRamo[ramo][servicio]["Costes"];
    var Impuestos = data.ServiciosRamo[ramo][servicio]["Impuestos"];
}

function ConstruirRowTarifaFinal(row) {
    var ramo = $(row).attr("id");
    var servicio = $(row).children().attr("id");
    var data = require('./js/DataProductos.json');
    var primaRacc = data.ServiciosRamo[ramo][servicio]["PrimaRACC"];
    var Costes = data.ServiciosRamo[ramo][servicio]["Costes"];
    var Impuestos = data.ServiciosRamo[ramo][servicio]["Impuestos"];
    var PrimaNeta = CalcularPrimaNeta(primaRacc, Costes);
    var rowTarifa = "<tr id='tariFin-" + ramo + "-" + servicio + "'>";
    rowTarifa += "<td class='darFormato' style='text-align:left;'>" + primaRacc + "</td>";
    rowTarifa += "<td class='darFormato' style='text-align:left;'>" + Costes + "</td>";
    rowTarifa += "<td class='darFormato' style='text-align:left;'>" + PrimaNeta + "</td>";
    rowTarifa += "<td class='darFormato' style='text-align:left;'>" + Impuestos + "</td>";
    rowTarifa += "<td  class='primaTotal darFormato' style='text-align:left;'>" + CalcularPrimaTotal(PrimaNeta, Impuestos) + "</td>";
    rowTarifa += "</tr>";
    $('#TablaTarificacionFinal tbody').append(rowTarifa);
}

function SeleccionarPrimaServicio(check) {
    var checkState = $(check).is(':checked');
    var inputPrima = $(check).parent().parent().parent().parent().find('td:eq(2)').children();
    var TotalPrima = $('#TotalPrima').val();
    if (checkState) {
        inputPrima.val($(check).val());
        $('#TotalPrima').val(parseFloat(TotalPrima) + parseFloat($(check).val()));
    }
    else {
        inputPrima.val("");
        $('#TotalPrima').val(parseFloat(TotalPrima) - parseFloat($(check).val()));
    }
}

function darformatoReturn(valor) {
    var nf = new Intl.NumberFormat("es-ES", {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    });
    return nf.format(valor);
}

function formatoAmericano(campo) {
    valor = $(campo).text();
    valor = valor.replace(/\./g, '');
    valor = valor.replace(',', '.');
    $(campo).text(valor);
}