$(document).ready(function () {
    //var data = require('./js/DataProductos.json');
    data = {
        "Ambitos": {
            "ambito1": {
                "descripcion": "Hogar",
                "modalidad1": {
                    "descripcion": "Asistencia Hogar",
                    "tipologia1": {
                        "descripcion": "Asistencia Hogar",
                        "cobertura1": {
                            "descripcion": "Servicio de asisténcia tecnológica",
                            "PrimaRiesgoAuto": 0.06,
                            "PrimaRiesgoGasto": 0.02,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.89,
                            "PrimaNetaManual": 0.00,
                            "PrimaComercialAuto": 0.10,
                            "PrimaComercialManual": 0.00,
                            "PrimaTotal": 1.05
                            
                        },
                        "cobertura2": {
                            "descripcion": "Servicio de inspección de instalaciones del hogar",
                            "PrimaRiesgoAuto": 0.30,
                            "PrimaRiesgoGasto": 0.09,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.39,
                            "PrimaNetaManual": 0.0,
                            "PrimaComercialAuto": 0.47,
                            "PrimaComercialManual": 0.0,
                            "PrimaTotal": 0.47
                        },
                        "cobertura3": {
                            "descripcion": "Servicios de reparaciones y reformas",
                            "PrimaRiesgoAuto": 0.16,
                            "PrimaRiesgoGasto": 0.04,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.09,
                            "PrimaNetaManual": 0.00,
                            "PrimaComercialAuto": 0.11,
                            "PrimaComercialManual": 0.00,
                            "PrimaTotal": 0.11
                        }
                    },
                    "tipologia2": {
                        "descripcion": "Asistencia Urgente al Hogar",
                        "cobertura4": {
                            "descripcion": "Servicio urgente de cristalería",
                            "PrimaRiesgoAuto": 1.43,
                            "PrimaRiesgoGasto": 0.00,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 5.29,
                            "PrimaNetaManual": 0.00,
                            "PrimaComercialAuto": 5.62,
                            "PrimaComercialManual": 0.00,
                            "PrimaTotal": 5.62
                        },
                        "cobertura5": {
                            "descripcion": "Servicio urgente de fontanería",
                            "PrimaRiesgoAuto": 0.16,
                            "PrimaRiesgoGasto": 0.04,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.09,
                            "PrimaNetaManual": 0.00,
                            "PrimaComercialAuto": 0.11,
                            "PrimaComercialManual": 0.00,
                            "PrimaTotal": 0.11
                        }
                    },
                }
            },

            "ambito2": {
                "descripcion": "Movilidad",
                "modalidad2": {
                    "descripcion": "Asistencia al vehículo",
                    "tipologia3": {
                        "descripcion": "VEHICULO DE SUBSTITUCION",
                        "cobertura6": {
                            "descripcion": "Vehículo de sustitución hasta 3 dias",
                            "PrimaRiesgoAuto": 0.30,
                            "PrimaRiesgoGasto": 0.09,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.39,
                            "PrimaNetaManual": 0.0,
                            "PrimaComercialAuto": 0.47,
                            "PrimaComercialManual": 0.0,
                            "PrimaTotal": 0.47

                        },
                        "cobertura7": {
                            "descripcion": "Vehículo de sustitución hasta 7 dias",
                            "PrimaRiesgoAuto": 0.06,
                            "PrimaRiesgoGasto": 0.02,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.89,
                            "PrimaNetaManual": 0.00,
                            "PrimaComercialAuto": 0.10,
                            "PrimaComercialManual": 0.00,
                            "PrimaTotal": 1.05
                        },
                        "cobertura8": {
                            "descripcion": "Vehículo de Alquiler por Accidente o Robo ",
                            "PrimaRiesgoAuto": 0.30,
                            "PrimaRiesgoGasto": 0.09,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.39,
                            "PrimaNetaManual": 0.0,
                            "PrimaComercialAuto": 0.47,
                            "PrimaComercialManual": 0.0,
                            "PrimaTotal": 0.47
                        }
                    },
                    "tipologia4": {
                        "descripcion": "ASISTENCIA MECANICA  ",
                        "cobertura9": {
                            "descripcion": " Asistencia Mecanica IN SITU ",
                            "PrimaRiesgoAuto": 0.04,
                            "PrimaRiesgoGasto": 0.00,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.15,
                            "PrimaNetaManual": 0.00,
                            "PrimaComercialAuto": 0.16,
                            "PrimaComercialManual": 0.00,
                            "PrimaTotal": 0.20
                        },
                        "cobertura10": {
                            "descripcion": "Remolcaje",
                            "PrimaRiesgoAuto": 0.06,
                            "PrimaRiesgoGasto": 0.02,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.89,
                            "PrimaNetaManual": 0.00,
                            "PrimaComercialAuto": 0.10,
                            "PrimaComercialManual": 0.00,
                            "PrimaTotal": 1.05
                        }
                    },
                },
                "modalidad3": {
                    "descripcion": "Asistencia a la bicicleta",
                    "tipologia5": {
                        "descripcion": "ASISTENCIA BICI",
                        "cobertura11": {
                            "descripcion": "Cambio de Rueda/Cadena ",
                            "PrimaRiesgoAuto": 0.06,
                            "PrimaRiesgoGasto": 0.02,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.89,
                            "PrimaNetaManual": 0.00,
                            "PrimaComercialAuto": 0.10,
                            "PrimaComercialManual": 0.00,
                            "PrimaTotal": 1.05

                        },
                        "cobertura12": {
                            "descripcion": "RC BICI 30.000 € ",
                            "PrimaRiesgoAuto": 0.04,
                            "PrimaRiesgoGasto": 0.00,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.15,
                            "PrimaNetaManual": 0.00,
                            "PrimaComercialAuto": 0.16,
                            "PrimaComercialManual": 0.00,
                            "PrimaTotal": 0.20
                        }
                    },
                    "tipologia6": {
                        "descripcion": "BICICLETA VIP",
                        "cobertura13": {
                            "descripcion": "Recurso Multas Bici ",
                            "PrimaRiesgoAuto": 1.43,
                            "PrimaRiesgoGasto": 0.00,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 5.29,
                            "PrimaNetaManual": 0.00,
                            "PrimaComercialAuto": 5.62,
                            "PrimaComercialManual": 0.00,
                            "PrimaTotal": 5.62
                        },
                        "cobertura14": {
                            "descripcion": "Falta batería/ Fallo motor eléctrico",
                            "PrimaRiesgoAuto": 0.06,
                            "PrimaRiesgoGasto": 0.02,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.89,
                            "PrimaNetaManual": 0.00,
                            "PrimaComercialAuto": 0.10,
                            "PrimaComercialManual": 0.00,
                            "PrimaTotal": 1.05
                        }
                    },
                }
            },


            "ambito3": {
                "descripcion": "Personas",
                "modalidad4": {
                    "descripcion": "Asistencia Personal ",
                    "tipologia7": {
                        "descripcion": "ASISTENCIA PERSONAL",
                        "cobertura15": {
                            "descripcion": "REPATRIACIÓN DIFUNTOS",
                            "PrimaRiesgoAuto": 1.43,
                            "PrimaRiesgoGasto": 0.00,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 5.29,
                            "PrimaNetaManual": 0.00,
                            "PrimaComercialAuto": 5.62,
                            "PrimaComercialManual": 0.00,
                            "PrimaTotal": 5.62

                        },
                        "cobertura16": {
                            "descripcion": "RETORNO ANTICIPADO",
                            "PrimaRiesgoAuto": 0.06,
                            "PrimaRiesgoGasto": 0.02,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.89,
                            "PrimaNetaManual": 0.00,
                            "PrimaComercialAuto": 0.10,
                            "PrimaComercialManual": 0.00,
                            "PrimaTotal": 1.05
                        },
                        "cobertura17": {
                            "descripcion": "ENVIO DE LLAVES",
                            "PrimaRiesgoAuto": 0.30,
                            "PrimaRiesgoGasto": 0.09,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.39,
                            "PrimaNetaManual": 0.0,
                            "PrimaComercialAuto": 0.47,
                            "PrimaComercialManual": 0.0,
                            "PrimaTotal": 0.47
                        }
                    },
                    "tipologia8": {
                        "descripcion": "TURISMO VIP  ",
                        "cobertura18": {
                            "descripcion": "Cursos Recuperación parcial o total Puntos  ",
                            "PrimaRiesgoAuto": 0.30,
                            "PrimaRiesgoGasto": 0.09,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.39,
                            "PrimaNetaManual": 0.0,
                            "PrimaComercialAuto": 0.47,
                            "PrimaComercialManual": 0.0,
                            "PrimaTotal": 0.47
                        }
                    },
                },
                "modalidad5": {
                    "descripcion": "Asistencia sanitaria",
                    "tipologia9": {
                        "descripcion": "ASISTENCIA SANITARIA",
                        "cobertura20": {
                            "descripcion": "TELMED",
                            "PrimaRiesgoAuto": 0.16,
                            "PrimaRiesgoGasto": 0.04,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.09,
                            "PrimaNetaManual": 0.00,
                            "PrimaComercialAuto": 0.11,
                            "PrimaComercialManual": 0.00,
                            "PrimaTotal": 0.11

                        },
                        "cobertura21": {
                            "descripcion": "Protección de Pagos",
                            "PrimaRiesgoAuto": 0.30,
                            "PrimaRiesgoGasto": 0.09,
                            "PrimaRiesgoManual": 0.00,
                            "PrimaNetaAuto": 0.39,
                            "PrimaNetaManual": 0.0,
                            "PrimaComercialAuto": 0.47,
                            "PrimaComercialManual": 0.0,
                            "PrimaTotal": 0.47
                        }
                    }
                }
            },
        }
    };


    //INICIO Primer pantalla
    $('#TablaAmbito tbody tr').click(function () {
        var ambitoID = $(this).children().attr("id");
        $.each(data.Ambitos, function (key, val) {
            if (key == ambitoID) {
                $('#TablaModalidad tbody').html("");
                $.each(val, function (servName, modalidad) {
                    if (modalidad.descripcion != undefined) $('#TablaModalidad tbody').append("<tr id='" + ambitoID + "'><td id='" + servName + "'>" + modalidad.descripcion + "</td></tr>");
                    $('#TablaTipologia tbody').html("");
                    $('#TablaCobertura tbody').html("");
                });
            }
        });

        $('#TablaModalidad tbody tr').click(function () {
            var modalidadID = $(this).children().attr("id");
            $.each(data.Ambitos[ambitoID], function (key, val) {
                if (key == modalidadID) {
                    $('#TablaTipologia tbody').html("");
                    $.each(val, function (servName, tipologia) {
                        if (tipologia.descripcion != undefined) $('#TablaTipologia tbody').append("<tr id='" + ambitoID + "-" + modalidadID + "'><td id='" + servName + "'>" + tipologia.descripcion + "</td></tr>");
                        $('#TablaCobertura tbody').html("");
                    });



                    $('#TablaTipologia tbody tr').click(function () {
                        var tipologiaID = $(this).children().attr("id");
                        var auxID = $(this).attr("id").split("-");
                        var ambitoID = auxID[0];
                        var modalidadID = auxID[1];

                        $.each(data.Ambitos[ambitoID][modalidadID], function (key, val) {
                            if (key == tipologiaID) {
                                $('#TablaCobertura tbody').html("");
                                $.each(val, function (servName, cobertura) {
                                    if (cobertura.descripcion != undefined) $('#TablaCobertura tbody').append("<tr onclick='SeleccionarCobertura(this)' id='" + ambitoID + "-" + modalidadID + "-" + tipologiaID + "-" + servName + "'><td id='" + servName + "'>" + cobertura.descripcion + "</td></tr>");
                                });
                            }
                        });
                    });
                }
            });
        });
    });

    //FIN Primer pantalla



    //BOTONES VOLVER Y SIGUIENTE
    $('#siguiente1').click(function () {
        $('#pantalla1').attr("hidden", true);
        $('#pantalla2').removeAttr("hidden");
        $('#pantalla3').attr("hidden", true);
        $('#pantalla4').attr("hidden", true);
    });
    $('#siguiente2').click(function () {
        $('#pantalla1').attr("hidden", true);
        $('#pantalla2').attr("hidden", true);
        $('#pantalla3').removeAttr("hidden");
        $('#pantalla4').attr("hidden", true);
    });
    $('#siguiente3').click(function () {
        $('#pantalla1').attr("hidden", true);
        $('#pantalla2').attr("hidden", true);
        $('#pantalla3').attr("hidden", true);
        $('#pantalla4').removeAttr("hidden");
    });
    $('#siguiente4').click(function () {
        alert('GUARDO CON EXITO')
    });
    $('#volver2').click(function () {
        $('#pantalla1').removeAttr("hidden");
        $('#pantalla2').attr("hidden", true);
        $('#pantalla3').attr("hidden", true);
        $('#pantalla4').attr("hidden", true);
    });
    $('#volver3').click(function () {
        $('#pantalla1').attr("hidden", true);
        $('#pantalla2').removeAttr("hidden");
        $('#pantalla3').attr("hidden", true);
        $('#pantalla4').attr("hidden", true);
    });
    $('#volver4').click(function () {
        $('#pantalla1').attr("hidden", true);
        $('#pantalla2').attr("hidden", true);
        $('#pantalla3').removeAttr("hidden");
        $('#pantalla4').attr("hidden", true);
    });





    $('#Finalizar').click(function () {
        $('#pantalla1').attr("hidden", true);
        $('#pantalla2').attr("hidden", true);
        $('#pantalla3').removeAttr("hidden");
        $('#pantalla4').attr("hidden", true);
        $('#pantalla5').attr("hidden", true);


        var sum = 0;
        // iterate through each td based on class and add the values
        $(".primaTotal").each(function () {

            var value = $(this).text();
            // add only if the value is number
            if (!isNaN(value) && value.length != 0) {
                sum += parseFloat(value);
            }
            $('#totalPrimaPrint').text(sum);
        });
        $('.darFormato').each(function () {
            var value = darformatoReturn($(this).text());
            $(this).text(value);
        });
    });

    $('#Calculo').click(function () {
        //$('.ShowAlfinalizar').attr("hidden", true);
        //$('.hiddeAlFinalizar').removeAttr("hidden");
        $('.darFormato').each(function () {
            formatoAmericano($(this));
        });
    });

    $('#Inicio').click(function () {
        $('.ShowAlTarificar').attr("hidden", true);
        $('.removeAlTarificar').removeAttr("hidden");
    });

    $('[data-toggle="tooltip"]').tooltip();
});

var data;







function SeleccionarCobertura(rowSelected) {
    var idVerificar = $(rowSelected).attr("id");
    var servicioVerificar = $(rowSelected).children().attr("id");
    if ($('#TablaProducto tbody tr#' + idVerificar + ' td#' + servicioVerificar).length <= 0) {
        var row = $(rowSelected).clone();
        $(row).attr("onClick", "QuitarCobertura(this)");
        $(row).attr("data-toggle", "tooltip");
        $(row).attr("data-placement", "top");
        $(row).attr("title", "Click para remover!");
        $(row).removeClass("ServicioIncluido");

        $('#TablaProducto tbody').append(row);

        var auxID = $(row).attr("id").split("-");
        var ambitoID = auxID[0];
        var modalidadID = auxID[1];
        var tipologiaID = auxID[2];
        var coberturaID = auxID[3];

        ConstruirRowPrimaRiesgo(ambitoID, modalidadID, tipologiaID, coberturaID);
        ConstruirRowModeloNegocio(ambitoID, modalidadID, tipologiaID, coberturaID);
        ConstruirRowProductoFinal(ambitoID, modalidadID, tipologiaID, coberturaID);


        $(rowSelected).addClass("ServicioIncluido");
    }
}
function QuitarCobertura(row) {
    debugger;

    var auxID = $(row).attr("id").split("-");
    //var ambitoID = auxID[0];
    //var modalidadID = auxID[1];
    //var tipologiaID = auxID[2];
    var coberturaID = auxID[3];

    $(row).remove();
    $('#TablaProductoFinal tbody tr#rowFinal' + coberturaID).remove();
    $('#TablaModeloNegocio tbody tr#rowModelo' + coberturaID).remove();
    $('#TablaPrimaRiesgo tbody tr#rowRiesgo' + coberturaID).remove();
}


function ConstruirRowPrimaRiesgo(ambitoID, modalidadID, tipologiaID, coberturaID) {
    var rowRiesgo = "<tr id='rowRiesgo" + coberturaID + "'>";
    rowRiesgo += "<td style='text-align:center;'>" + data.Ambitos[ambitoID].descripcion  + "</td>";
    rowRiesgo += "<td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["descripcion"] + "</td>";
    rowRiesgo += "<td style='text-align:center;'>" + ConstruirDropdownlist(coberturaID, "perfilSocio", optionPerfilSocio) + "</td>";
    rowRiesgo += "<td style='text-align:center;'>" + ConstruirInput(coberturaID, "primaRiesgoFrecuencia") + "</td>";
    rowRiesgo += "<td style='text-align:center;'>" + ConstruirInput(coberturaID, "primaRiesgoCosteMedio") + "</td>";
    rowRiesgo += "<td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["PrimaRiesgoAuto"] + "</td>";
    rowRiesgo += "<td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["PrimaRiesgoGasto"] + "</td>";
    rowRiesgo += "<td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["PrimaRiesgoManual"] + "</td>";
    rowRiesgo += "<td></td></tr>";


    $('#TablaPrimaRiesgo tbody').append(rowRiesgo);
}
function ConstruirRowModeloNegocio(ambitoID, modalidadID, tipologiaID, coberturaID) {
    var rowModelo = "<tr id='rowModelo" + coberturaID + "'>";
    rowModelo += "<td style='text-align:center;'>" + data.Ambitos[ambitoID].descripcion + "</td>";
    rowModelo += "<td>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["descripcion"] + "</td>";
    rowModelo += "<td style='text-align:center;'>" + ConstruirDropdownlist(coberturaID, "modeloNegocio", optionModeloNegocio) + "</td>";
    rowModelo += "<td style='text-align:center;'>" + ConstruirInput(coberturaID, "seguroGastoInterno") + "</td>";
    rowModelo += "<td style='text-align:center;'>" + ConstruirInput(coberturaID, "seguroRACC") + "</td>";
    rowModelo += "<td style='text-align:center;'>" + ConstruirInput(coberturaID, "seguroAdquisicion") + "</td>";
    rowModelo += "<td style='text-align:center;'>" + ConstruirInput(coberturaID, "seguroMargen") + "</td>";
    rowModelo += "<td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["PrimaNetaAuto"] + "</td>";
    rowModelo += "<td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["PrimaNetaManual"] + "</td>";
    //rowModelo += "<td></td><td style='text-align:center;'>" + ConstruirInput(coberturaID, "serviciosGastoInterno") + "</td>";
    //rowModelo += "<td style='text-align:center;'>" + ConstruirInput(coberturaID, "serviciosRACC") + "</td>";
    //rowModelo += "<td style='text-align:center;'>" + ConstruirInput(coberturaID, "serviciosAdquisicion") + "</td>";
    //rowModelo += "<td style='text-align:center;'>" + ConstruirInput(coberturaID, "serviciosMargen") + "</td>";
    //rowModelo += "<td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["ServiciosPrimaNetaAuto"] + "</td>";
    //rowModelo += "<td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["ServiciosPrimaNetaManual"] + "</td>";
    rowModelo += "<td></td></tr>";

    $('#TablaModeloNegocio tbody').append(rowModelo);
}
function ConstruirRowProductoFinal(ambitoID, modalidadID, tipologiaID, coberturaID) {
    var rowFinal = "<tr id='rowFinal" + coberturaID + "'>";
    rowFinal += "<td style='text-align:center;'>" + data.Ambitos[ambitoID].descripcion + "</td>";
    rowFinal += "<td>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["descripcion"] + "</td>";
    rowFinal += "<td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["PrimaRiesgoAuto"] + "</td>";
    rowFinal += "<td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["PrimaRiesgoGasto"] + "</td>";
    rowFinal += "<td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["PrimaRiesgoManual"] + "</td>";
    rowFinal += "<td></td><td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["PrimaNetaAuto"] + "</td>";
    rowFinal += "<td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["PrimaNetaManual"] + "</td>";
    //rowFinal += "<td></td><td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["ServiciosPrimaNetaAuto"] + "</td>";
    //rowFinal += "<td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["ServiciosPrimaNetaManual"] + "</td>";
    rowFinal += "<td></td><td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["PrimaComercialAuto"] + "</td>";
    rowFinal += "<td style='text-align:center;'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["PrimaComercialManual"] + "</td>";

    rowFinal += "<td style='text-align:center; font-weight:bold'>" + data.Ambitos[ambitoID][modalidadID][tipologiaID][coberturaID]["PrimaTotal"] + "</td>";

    //rowFinal += "<td  class='primaTotal darFormato' style='text-align:left;'>" + CalcularPrimaTotal(PrimaNeta, Impuestos) + "</td>";
    rowFinal += "<td></td></tr>";
    $('#TablaProductoFinal tbody').append(rowFinal);
}


var optionModeloNegocio = '<option selected="selected" value=1>Prestación de servicios</option><option value=2>Seguro</option>';
var optionPerfilSocio = '<option selected="selected" value=1>Socio</option><option value=2>No Socio</option>';
function ConstruirDropdownlist(coberturaID, nombreAux, option) {
    var ddl = '<select class="rs-select2--light rs-select2--md" id="' + nombreAux + coberturaID + '" name="' + nombreAux + coberturaID + '">' + option + '</select>';
    return ddl;
}
function ConstruirInput(coberturaID, nombreAux) {
    var input = "<input type='text' style='width:110px; height:25px; ' id='" + nombreAux + coberturaID + "' name='" + nombreAux + coberturaID + "' >";
    return input;
}







function CalcularPrimaNeta(primaRiesgo, Costes) {
    return parseFloat(primaRiesgo) + parseFloat(Costes);
}
function CalcularPrimaTotal(primaNeta, Impuestos) {
    return parseFloat(primaNeta) + parseFloat(Impuestos);
}



function ContruirInputPrimaServicio(servicio) {
    var input = "<input type='text' style='width:110px; height:25px; border:solid 1px black;'>";
    return input;
}

//function ConstruirCheckBox(prima, servicio, ramo) {
//    var check = '<input onchange="SeleccionarPrimaServicio(this)" type="checkbox" id="' + servicio + '" name="' + ramo + "-" + servicio + '" value="' + prima + '" class="form-check-input">';
//    return check;
//}



function UpdateRowTarifaFinal(primaRiesgo, row) {
    //var data = require('./js/DataProductos.json');
    var Costes = data.Ambitos[ramo][servicio]["Costes"];
    var Impuestos = data.Ambitos[ramo][servicio]["Impuestos"];
}



function SeleccionarPrimaServicio(check) {
    var checkState = $(check).is(':checked');
    var inputPrima = $(check).parent().parent().parent().parent().find('td:eq(2)').children();
    var TotalPrima = $('#TotalPrima').val();
    if (checkState) {
        inputPrima.val($(check).val());
        $('#TotalPrima').val(parseFloat(TotalPrima) + parseFloat($(check).val()));
    }
    else {
        inputPrima.val("");
        $('#TotalPrima').val(parseFloat(TotalPrima) - parseFloat($(check).val()));
    }
}

function darformatoReturn(valor) {
    var nf = new Intl.NumberFormat("es-ES", {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    });
    return nf.format(valor);
}

function formatoAmericano(campo) {
    valor = $(campo).text();
    valor = valor.replace(/\./g, '');
    valor = valor.replace(',', '.');
    $(campo).text(valor);
}